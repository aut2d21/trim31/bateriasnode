const express = require('express');
const app = express();
const path = require('path');
const port = 3000;

app.use(express.static('public')); // Pasta onde você vai armazenar os arquivos HTML e estáticos

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.get('/conclusoes', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'conclusoes.html'));
});

app.get('/medicoes', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'medicoes.html'));
});

app.get('/procedimentos', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'procedimento.html'));
});

app.get('/teoria', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'teoria.html'));
});

app.listen(port, () => {
    console.log(`Servidor rodando em http://localhost:${port}`);
});
